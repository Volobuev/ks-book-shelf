const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function (env, paths) {
    return merge([
        {
            entry: paths.source + '/ts/index.tsx',
            module: {
                rules: [
                    {
                        test: /\.ts(x?)$/,
                        loader: ['babel-loader', 'awesome-typescript-loader'],
                        exclude: /node_modules/
                    },
                    {
                        test: /\.js$/,
                        enforce: 'pre',
                        loader: 'source-map-loader'
                    }
                ]
            },
            resolve: {
                extensions: ['.tsx', '.ts', '.js', '.jsx']
            },
            output: {
                path: paths.build,
                filename: 'js/[name].js',
                publicPath: '/'
            },
            optimization: {
                splitChunks: {
                  cacheGroups: {
                    commons: {
                      test: /[\\/]node_modules[\\/]/,
                      name: 'vendors',
                      chunks: 'all'
                    }
                  }
                }
            },
            plugins: [
                new CleanWebpackPlugin([paths.build], {
                    root: paths.root
                }),
                new HtmlWebpackPlugin({
                    inject: true,
                    hash: true,
                    template: 'src/index.html'
                }),
                new webpack.SourceMapDevToolPlugin({
                    filename: '[name].js.map',
                    test: /\.js$/
                }),
                new CopyWebpackPlugin([
                    { from: paths.source + '/assets/*', to: paths.build + '/assets', flatten: true },
                    { from: paths.source + '/assets/fonts', to: paths.build + '/assets/fonts', flatten: true }
                  ], {})
            ],
            devServer: {
                contentBase: paths.build,
                compress: true,
                port: 9000
            }
        }
    ])
};
