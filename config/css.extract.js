const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function (paths) {
    return {
        module: {
            rules: [
                {
                    test: /\.(css|sass|scss)$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 2,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                },
                {
                    test: /\.woff2?$|\.ttf$|\.eot$|\.svg$$/,
                    loader: 'file-loader',
                    options: {
                        name: 'css/fonts/[hash].[ext]',
                        publicPath: function(url) {
                            return url.replace(/css/, '.')
                        },
                    }  
                }
            ],
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "css/[name].css",
                chunkFilename: "css/[id].css"
              })
        ],
    };
};
