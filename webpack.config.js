const path = require('path');
const merge = require('webpack-merge');
const common = require('./config/common');
const extractCSS = require('./config/css.extract');

const PATHS = {
    root: path.join(__dirname),
    source: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'public')
};

module.exports = function (env) {
    return merge([
        extractCSS(PATHS),
        common(env, PATHS),
    ]);
};
