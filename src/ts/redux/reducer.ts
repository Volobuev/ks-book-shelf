import { handleActions } from 'redux-actions';
import ActionTypes from './types';
import { IState } from '../interfaces';
import * as _ from 'lodash';
import { SORTING_DIRECTION_ASC } from '../constants/book';

const initialState: IState = {
  loading: false,
  books: [],
  sorters: [
    { title: 'название', fieldName: 'title', direction: SORTING_DIRECTION_ASC, isActive: true },
    { title: 'год публикации', fieldName: 'publishYear', direction: SORTING_DIRECTION_ASC, isActive: false }
  ]
};

const mainReducer = handleActions<any, any>(
  {
    [ActionTypes.getBooks(null).type]: (state: any, action: any): any => {
      const result = { ...state };
      _.assign(result, { books: action.payload });
      
      return result;
    },
    [ActionTypes.setSorters(null).type]: (state: any, action: any): any => {
      const result = { ...state };
      _.assign(result, { sorters: action.payload });

      return result;
    }
  },
  initialState,
);

export default mainReducer;
