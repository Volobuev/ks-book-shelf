import { createAction } from 'redux-actions';

class ActionTypes {
    static getBooks = createAction<any>(`GET_BOOKS_LIST`);
    static setSorters = createAction<any>(`SORT_BOOKS`);
}

export default ActionTypes;
