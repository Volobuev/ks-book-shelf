import ActionTypes from './types';
import store from './store';
import LocalStorageService from '../services/local-storage-service';
import { BOOK_STORAGE_ITEM, SORTERS_STORAGE_ITEM } from '../constants/book';
import * as _ from 'lodash';
import { Guid } from '../services/guid';

class Actions {
  protected static dataService = new LocalStorageService();
  static getBooks = () => {
    //store.dispatch(ActionTypes.loading(true));
    let books = Actions.dataService.getEntities(BOOK_STORAGE_ITEM);
    let sorters = Actions.dataService.getEntities(SORTERS_STORAGE_ITEM);
    if (sorters && sorters.length > 0) {
      let activeSorters = _.filter(sorters, (el) => el.isActive)
      books = _.orderBy(books, activeSorters.map(el => el.fieldName), activeSorters.map(el => el.direction.toLowerCase()));
      store.dispatch(ActionTypes.setSorters(sorters));
    }

    store.dispatch(ActionTypes.getBooks(books));
  }
  static sortBooks = (sorters) => {
    Actions.saveSorters(sorters);
    Actions.getBooks();
  }
  static getBook = (id) => {
    let books = Actions.dataService.getEntities(BOOK_STORAGE_ITEM);
    
    return _.find(books, (el) => Guid.compare(el.id, id));
  }
  static saveBook = (item) => {
    let books = Actions.dataService.getEntities(BOOK_STORAGE_ITEM);
    
    let existingElementId = _.indexOf(books, _.find(books, (el) => Guid.compare(el.id, item.id)));
    if (existingElementId >= 0) {
      books.splice(existingElementId, 1, item);
    } 
    else {
      books.push(item);
    }

    Actions.dataService.saveEntities(BOOK_STORAGE_ITEM, books);
    //store.dispatch(ActionTypes.getBooks(books));
  }
  static removeBook = (id) => {
    let books = Actions.dataService.getEntities(BOOK_STORAGE_ITEM);
    let existingElementId = _.indexOf(books, _.find(books, (el) => Guid.compare(el.id, id)));
    if (existingElementId >= 0) {
      books.splice(existingElementId, 1);
    }
    Actions.dataService.saveEntities(BOOK_STORAGE_ITEM, books);
    store.dispatch(ActionTypes.getBooks(books));
  }
  static saveSorters = (items) => {
    Actions.dataService.saveEntities(SORTERS_STORAGE_ITEM, items);
  }
}

export default Actions;
