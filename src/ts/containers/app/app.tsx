import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import ContentComponent from '../content/content';
import MenuComponent from '../menu/menu';
import { IState } from '../../interfaces';

const App = () => (
    <div className="ks-app">
        <div className="ks-app__body">
            <MenuComponent></MenuComponent>
            <ContentComponent></ContentComponent>
        </div>
    </div>
);

const mapper = (state, selfProps) => {
    return {
        ...selfProps,
        loading: state.loading,
        books: state.books,
        sorters: state.sorters
    };
}

export default withRouter(connect<any, any, IState>(mapper)(App));
