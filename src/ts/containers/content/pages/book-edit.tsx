import * as React from 'react';
import { default as BookFormComponent } from '../component/book-form';

const BookEditComponent = (props) => {
    return (
      <BookFormComponent isNew={false} {...props}/>
    );
  }
export default BookEditComponent;
