import * as React from 'react';
import { default as BookFormComponent } from '../component/book-form';

const BookСreateComponent = (props) => {
    return (
      <BookFormComponent isNew={true} {...props}/>
    );
  }
export default BookСreateComponent;
