import * as React from 'react';
import { withRouter } from 'react-router-dom';
import { IState } from '../../../interfaces';
import { connect } from 'react-redux';
import Actions from '../../../redux/actions';
import BookViewComponent from '../component/book-view';
import { SORTING_DIRECTION_ASC, SORTING_DIRECTION_DESC } from '../../../constants/book';
import * as _ from 'lodash';

class BookListComponent extends React.Component<any, IState> {
    constructor(props: any)
    {
        super(props);
    }

    componentWillMount() {
        Actions.getBooks();
    }

    componentWillReceiveProps(nextProps: any) { 
        //console.log(nextProps);
    }

    routeTo = (id) => {
        let href = `/edit/${id}`;

        //this for Browser router, but it's not good for spa, because of direct link /create or smth like that doesn't work properly
        //because of html5 history api
        /*if (window.location.pathname != href)
            this.props.history.push(href);*/

        if (('#' + href) != window.location.hash) { 
            this.props.history.push(href); 
        }
    }

    removeBook = (id) => {
        Actions.removeBook(id);      
    }

    sortBy = (index, direction) => {
        let newSorters = [ ...this.props.sorters ];
        _.forEach(newSorters, (item, key) => {
            item.isActive = false;
        });

        let sorter =  newSorters[index];
        sorter.isActive = true;
        sorter.direction = direction == SORTING_DIRECTION_ASC ? SORTING_DIRECTION_DESC : SORTING_DIRECTION_ASC;
        newSorters.splice(index, 1, sorter);
        
        Actions.sortBooks(newSorters); 
    }

    render() {
        let booksList = this.props.books;

        if (booksList && booksList.length > 0) {
            return (
                <div>
                    <h1>Библиотека</h1>
                    <div className="ks-list__sorting">
                        {
                            this.props.sorters && this.props.sorters.map((item, index) => {
                                return <div key={index} onClick={() => this.sortBy(index, item.direction)}>
                                    <span>{item.title}</span>
                                    {
                                        item.isActive &&
                                        <span className='material-icons'>{ item.direction == SORTING_DIRECTION_ASC ? `arrow_drop_up` : `arrow_drop_down`}</span>
                                    }
                                </div>
                            })
                        }
                    </div>
                    <div className="ks-list">
                    {
                        booksList.map((book, index) => 
                            <BookViewComponent key={index} bookInfo={book} onClick={this.routeTo} onRemove={this.removeBook} />
                        )
                    }
                    </div>
                </div>
            );
        }
        else {
            return (
                <div>
                    <h1>Библиотека</h1>
                    <div>Нет данных</div>
                </div>
            );
        }
    }
}

const mapper = (state, selfProps) => {
    return {
        ...selfProps,
        loading: state.loading,
        books: state.books,
        sorters: state.sorters
    };
}

export default withRouter(connect<any, any, any>(mapper)(BookListComponent));
