import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import * as pages from './pages';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

class ContentComponent extends React.Component {
    render() {
        return (
            <div className="ks-content">
                <Switch>
                    <Route exact path='/' component={pages.BookListComponent} />
                    <Route path='/edit/:id' component={pages.BookEditComponent} />
                    <Route path='/create' component={pages.BookCreateComponent} />
                </Switch>
                <div className="ks-facade ks-facade--tright"></div>
                <div className="ks-facade ks-facade--tleft" ></div>
            </div>
        )
    }
}

const mapper = (state, selfProps) => {
    return {
        ...selfProps,
        loading: state.loading,
        books: state.books,
        sorters: state.sorters
    };
}

export default withRouter(connect<any, any, any>(mapper)(ContentComponent));
