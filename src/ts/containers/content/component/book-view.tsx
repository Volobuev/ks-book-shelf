import * as React from 'react';
import { IBookEntry } from "../../../interfaces";

interface IBookViewProps {
    bookInfo: IBookEntry;
    onClick: Function;
    onRemove: Function;
};

export default class BookViewComponent extends React.PureComponent<IBookViewProps> {
    handleClick = (book) => {
        if (!this.props.onClick) {
          return;
        }
        
        this.props.onClick(book.id);
    }

    handleRemove = (ev, book) => {
        ev.stopPropagation();
        if (!this.props.onRemove) {
          return;
        }
    
        this.props.onRemove(book.id);
    }
    
    render() {
        let book = this.props.bookInfo;

        let bookCover = <span className='material-icons md-48'>panorama</span>;
        if (book && book.icon && book.icon.uri) {
            bookCover = <img src={book.icon.uri} />;
        }

        return (
            <div className="ks-row" onClick={() => this.handleClick(book)}>
                {
                    <div className="ks-book-img">
                        {bookCover}
                    </div>
                }
                <div className="ks-book-descr">
                    {
                        book && book.title &&
                        <div>
                            <span>{`Название:`}</span>
                            <span>{`${book.title.toUpperCase()}`}</span>
                        </div>
                    }
                    {
                        book.authors && book.authors.length > 0 && <div>
                            <span>{`Авторы:`}</span>
                            <span>{book.authors.map((val) => `${val.firstName.toUpperCase()} ${val.secondName.toUpperCase()}`).join(', ')}</span>
                        </div>
                    }
                    {
                        book && book.pagesCount && 
                        <div>
                            <span>{`Количество страниц:`}</span>
                            <span>{`${book.pagesCount}`}</span>
                        </div>
                    }
                    {
                        book && book.issuerTitle && 
                        <div>
                            <span>{`Название издательства:`}</span>
                            <span>{`${book.issuerTitle}`}</span>
                        </div>
                    }
                    {
                        book && book.publishYear && 
                        <div>
                            <span>{`Год публикации:`}</span>
                            <span>{`${book.publishYear}`}</span>
                        </div>
                    }
                    {
                        book && book.issueDate && 
                        <div>
                            <span>{`Дата выхода в тираж:`}</span>
                            <span>{`${book.issueDate}`}</span>
                        </div>
                    }
                    {
                        book && book.isbn && 
                        <div>
                            <span>{`ISBN:`}</span>
                            <span>{`${book.isbn}`}</span>
                        </div>
                    }
                </div>
                <div className="ks-remove-action-btn">
                    <i className="material-icons" onClick={(ev) => this.handleRemove(ev, book)}>delete</i>
                </div>
            </div>
        );
    }
}
