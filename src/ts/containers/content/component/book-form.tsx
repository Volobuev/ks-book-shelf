import * as React from 'react';
import { withRouter } from 'react-router-dom';
import { IBookEntry } from '../../../interfaces';
import { Guid } from '../../../services/guid';
import ButtonComponent from '../../../components/button/button';
import FileFieldComponent from '../../../components/file-field/file-field';
import Actions from '../../../redux/actions';
import IsbnValidator from '../../../validators/isbn-validator';
import NumberRangeValidator from '../../../validators/number-range-validator';
import DateValidator from '../../../validators/date-validator';
import RequiredValidator from '../../../validators/required-validator';
import * as _ from 'lodash';

interface ICreateBookState extends IBookEntry {}

class BookFormComponent extends React.Component<any, ICreateBookState> {
    validators: any = {};
    
    state: ICreateBookState = {
        id: Guid.getNewGUIDString(),
        authors: [],
        title: '',
        pagesCount: null,
        issuerTitle: '',
        publishYear: null,
        issueDate: '',
        isbn: '',
        icon: null
    };

    constructor(props: any)
    {
        super(props);

        if (props && !props.isNew) {
            let id = props.match.params.id;
            this.state = { ...Actions.getBook(id) };
        }

        this.validators['pagesCount'] = [{ message: 'Не более 10000', validator: new NumberRangeValidator(0, 10000) }];
        this.validators['publishYear'] = [{ message: 'Не ранее 1800', validator: new NumberRangeValidator(1800) }];
        this.validators['isbn'] = [{ message: 'Неверное значение ISBN', validator: new IsbnValidator() }];
        this.validators['issueDate'] = [{ message: 'Неверное значение даты (формат dd.mm.yyyy и не ранее 01.01.1800)', validator: new DateValidator() }];
    }

    isFormValid = (): boolean => {
        let requiredValidator = new RequiredValidator();
        if (!requiredValidator.validate(this.state.title))
        {
            return false;
        }

        if (!requiredValidator.validate(this.state.pagesCount))
        {
            return false;
        }

        if (this.state.authors.length == 0)
        {
            return false;
        }

        if (this.state.authors.length != 0) {
            let someEmptyAuthorInfo = _.some(this.state.authors, (value) => {
                return !requiredValidator.validate(value.firstName) || !requiredValidator.validate(value.secondName)
            });

            return !someEmptyAuthorInfo;
        }

        return true;
    }

    save = () => {
        Actions.saveBook(this.state);
        alert('Данные изменены');
        if ((`#/edit/${this.state.id}`) != window.location.hash) { 
            this.props.history.push(`/edit/${this.state.id}`);
        }
    } 

    validate = (fieldName): string[] => {
        let validationResult = [];
        if (this.state[fieldName]) {
            this.validators[fieldName].map((v) => {
                if(!v.validator.validate(this.state[fieldName])) {
                    validationResult.push(v.message);
                }
            });
        }

        return validationResult;
    }

    uploadImage = (file) => {
        if (file) {
            if (FileReader && file) {
                var fileReader = new FileReader();
                fileReader.onload = () => {
                    let dataUri = `${fileReader.result}`;
                    this.setState({ icon: { name: file.name, uri: dataUri}});
                }
                fileReader.readAsDataURL(file);
            }
            else {
                console.log('No support for File API');
            }
        } else {
            this.setState({ icon: null });
        }
    }

    newAuthor = () => {
        let newAuthors =  [ ...this.state.authors ];
        newAuthors.push({ firstName: '', secondName: '' });

        this.setState({authors: newAuthors});
    }

    removeAuthor = (index) => {
        let newAuthors =  [ ...this.state.authors ];
        newAuthors.splice(index, 1);

        this.setState({authors: newAuthors});
    }

    updateAuthor = (index, fieldName, value) => {
        let author =  this.state.authors[index];
        author[fieldName] = value;

        let newAuthors =  [ ...this.state.authors ];
        newAuthors.splice(index, 1, author);

        this.setState({authors: newAuthors});
    }

    render() {
        return (<div>
            <h1>Добавить новую книгу</h1>

            <div className="ks-book-wrapper">
                {
                    this.state.icon &&
                    <div className="ks-book-img">
                        <img src={this.state.icon.uri}></img>
                    </div>
                }
                <div className="ks-book-info">
                    <div>
                        <span>Название*</span>
                        <span><input type="text" maxLength={30} value={this.state.title} required={true} onChange={(val) => this.setState({title: val.target.value})} /></span>
                    </div>
                    <div>
                        <span>Авторы*</span>
                        <div className="ks-book-info__author">
                            {
                                this.state.authors && this.state.authors.length <= 5 &&
                                <span className="add-btn material-icons" onClick={() => this.newAuthor()}>add_circle_outline</span>
                            }
                            {
                                this.state.authors && 
                                this.state.authors.map((item, index) => {
                                    return <div className="ks-book-info__author__item" key={index}>
                                        <span className="material-icons remove-btn" onClick={() => this.removeAuthor(index)}>remove</span>
                                        <input  type="text" placeholder={`ИМЯ`} 
                                                maxLength={20} value={item.firstName} 
                                                required={true} onChange={(val) => this.updateAuthor(index, 'firstName', val.target.value)} />
                                        <input  type="text" placeholder={`ФАМИЛИЯ`} 
                                                maxLength={20} value={item.secondName} 
                                                required={true} onChange={(val) => this.updateAuthor(index, 'secondName', val.target.value)} />
                                    </div>
                                })
                            }
                        </div>
                    </div>
                    <div>
                        <span>Количество страниц*</span>
                        <span><input type='number' value={this.state.pagesCount} required={true} pattern={`[0-9]{4}`} min={0} max={10000} onChange={(val) => this.setState({pagesCount: Number(val.target.value)})} /></span>
                        {this.validate('pagesCount').map((errorMessage, index) => <span key={index} style={{color: 'red'}}>{errorMessage}</span>)}
                    </div>
                    <div>
                        <span>Название издательства</span>
                        <span><input type="text" maxLength={30} value={this.state.issuerTitle} onChange={(val) => this.setState({issuerTitle: val.target.value})} /></span>
                    </div>
                    <div>
                        <span>Год публикации</span>
                        <span><input type='number' value={this.state.publishYear} pattern={`[0-9]{4}`} min={1800} onChange={(val) => this.setState({publishYear: Number(val.target.value)})} /></span>
                        {this.validate('publishYear').map((errorMessage, index) => <span key={index} style={{color: 'red'}}>{errorMessage}</span>)}
                    </div>
                    <div>
                        <span>Дата выхода в тираж</span>
                        <span>
                            <input type='text' placeholder={`DD.MM.YYYY`}  value={this.state.issueDate} onChange={(val) => this.setState({issueDate: val.target.value})} />
                            {this.validate('issueDate').map((errorMessage, index) => <span key={index} style={{color: 'red'}}>{errorMessage}</span>)}
                        </span>                        
                    </div>
                    <div>
                        <span>ISBN</span>
                        <span><input type="text" value={this.state.isbn} required={false} onChange={(val) => this.setState({isbn: val.target.value})} /></span>
                        {this.validate('isbn').map((errorMessage, index) => <span key={index} style={{color: 'red'}}>{errorMessage}</span>)}
                    </div>
                    <div>
                        <span>Изображение книги</span>
                        <FileFieldComponent title="(png, jpg, ico)" onChange={this.uploadImage} accept=".jpg,.png,.ico" />
                    </div>
                    <div style={{ marginTop: 24 }}>
                        <ButtonComponent title="Сохранить" onClick={this.save} isDisabled={!this.isFormValid()} />
                    </div>
                </div>
            </div>
        </div>);
    }
}
export default withRouter(BookFormComponent);
