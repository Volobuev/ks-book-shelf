import * as React from 'react';
import { withRouter } from 'react-router-dom';

class MenuComponent extends React.Component<any> {
    routeTo(href: string) {
        /*if (window.location.pathname != href)
            this.props.history.push(href);*/
        
        if (('#' + href) != window.location.hash) { 
            this.props.history.push(href); 
        }
    }
    render() {
        return <div className="ks-menu">
            <div className="ks-menu__item" onClick={() => this.routeTo('/')}>
                <span className="material-icons">search</span>
            </div>
            <div className="ks-menu__item" onClick={() => this.routeTo('/create')}>
                <span className="material-icons">add</span>
            </div>
        </div>;
    }
}

export default withRouter(MenuComponent);
