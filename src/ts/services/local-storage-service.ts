import IStorageService from "./storage-service";

export default class LocalStorageService implements IStorageService {
    saveEntities(storageItemName: string, entities: any[]) {
        if (localStorage) {
            localStorage.setItem(storageItemName, JSON.stringify(entities));
        }
    }

    getEntities (storageItemName: string) {
        if (localStorage) {
            let entities = localStorage.getItem(storageItemName);
            return entities && entities.length > 0 ? JSON.parse(entities) : [];
        }

        return [];
    }

    removeEntities (storageItemName: string) {
        if (localStorage) {
            localStorage.removeItem(storageItemName);
        }
    }
}
