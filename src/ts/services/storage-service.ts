export default interface IStorageService {
    saveEntities(storageItemName: string, entities: any[]): void,
    getEntities(storageItemName: string): any[],
    removeEntities(storageItemName: string): void
}
