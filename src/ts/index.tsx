import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/app';
import { HashRouter  } from 'react-router-dom';
import store from './redux/store';
import 'core-js';

import '../styles/main.scss';

ReactDOM.render(
  <Provider store={store}>
    <HashRouter basename="/" hashType="slash">
        <App></App>
    </HashRouter>
  </Provider>,
  document.getElementById('app'),
);
