import IAuthor from "./author";
import IFileInfo from "./file";

export default interface IBookEntry {
    id: string;
    title: string;
    authors?: IAuthor[] | any;
    pagesCount: number | any;
    issuerTitle?: string;
    publishYear?: number | any;
    issueDate?: string | any;
    isbn?: string;
    icon?: IFileInfo | any;
}
