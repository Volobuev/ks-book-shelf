export default interface IFileInfo {
    name: string;
    uri: string;
}
