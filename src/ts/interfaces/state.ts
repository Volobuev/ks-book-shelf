import { IBookEntry, ISorter } from '../interfaces';

export default interface IState {
    loading: boolean;
    books: IBookEntry[],
    sorters?: ISorter[]
}
