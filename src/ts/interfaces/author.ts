export default interface IAuthor {
    firstName: string;
    secondName: string;
}
