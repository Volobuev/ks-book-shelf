export default interface ISorter {
    title: string;
    fieldName: string;
    direction: string;
    isActive?: boolean;
}
