import * as React from 'react';

interface IButtonProps {
  title: string;
  onClick: Function;
  isDisabled?: boolean;
}

export default class ButtonComponent extends React.PureComponent<IButtonProps> {

  static defaultProps: IButtonProps = {
    onClick: null,
    title: 'Отправить',
    isDisabled: false
  };

  handleClick = () => {
    if (this.props.isDisabled || !this.props.onClick) {
      return;
    }

    this.props.onClick();
  }

  render() {

    const disabledStyleClass = this.props.isDisabled && 'ks-button--disabled';
    const styleClasses = `ks-button ${disabledStyleClass}`;

    return (
      <div className={styleClasses} onClick={this.handleClick}>
        <div className="ks-button__title">{this.props.title}</div>
      </div>
    );
  }
}

