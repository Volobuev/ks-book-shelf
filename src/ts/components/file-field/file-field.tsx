import * as React from 'react';

interface IFileFieldProps {
    onChange?: Function;
    accept?: string;
    title: string;
}

interface IFileFieldState {
    fileName?: string;
}

export default class FileFieldComponent extends React.PureComponent<IFileFieldProps, IFileFieldState> {
  state: IFileFieldState = {
    fileName: '',
  };
  inputField: HTMLElement = null;
  files: any = null;
  handleChange = (event: any) => {
    this.files = event.target.files;
    let fileName = '';

    if (this.files.length === 0) {
      fileName = 'Файл не выбран';
    } else {
      fileName = this.files[0].name;
    }

    this.setState({ fileName });

    if (this.props.onChange) {
      this.props.onChange(this.files[0]);
    }
  }

  handleClickButton = () => {
    this.inputField.click();
  }

  render() {
    return (
      <div className="ks-file-field">
        <div className="ks-file-control" onClick={this.handleClickButton}>
            <span className="material-icons">file_upload</span>
            <span style={{ marginRight: 8 }}>{this.props.title}</span>
            <div className="file-control__title">{this.state.fileName}</div>
        </div>
        <div className="ks-file-field__native">
          <input type="file" multiple={false} ref={el => this.inputField = el} onChange={this.handleChange} accept={this.props.accept} />
        </div>
      </div>
    );
  }
}
