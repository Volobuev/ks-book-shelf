import IValidator from "./validator";

/**
 * ISBN-10 and ISBN-13 validation
 * Allow parts be separated by hyphens or spaces, or none at all:
 * ISBN-13:
 *  978-0-596-52068-7
 *  978 0 596 52068 7
 *  9780596520687
 * 
 * ISBN-10:
 * 0-596-52068-9
 * 0 596 52068 9
 * 0596520689
 */
export default class IsbnValidator implements IValidator {
    /**
     * Regular expression to validate format of the ISBN
     */
    //private regexp = /^(?:\d{9}[\dXx]|\d{13})$/;//only with no delimeters
    private regexp = /^(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;

    /**
     * You cannot validate an ISBN using a regex alone, because the last digit is computed using a checksum algorithm. 
     * So these method valuate check sum and compare its (value mod 11) with last digit
     */
    private checksum = (isbn: string): boolean => {
        //remove hyphens and spaces
        let sanitizeIsbn = isbn.replace(/[- ]/g, '');

        //remove last digit (control digit)
        let isbnNumber = sanitizeIsbn.slice(0, -1);
        
        //convert number to array (with only digits)
        let isbnDigits = isbnNumber.split('').map(Number);
        
        //Save last digit (control digit):
        const last = sanitizeIsbn.slice(-1);
        const lastDigit = (last !== 'X') ? parseInt(last, 10) : 'X';

        if (isbnDigits.length == 9) //then isbn-10 checksum algorithm
        {
            //check digit calculation
            // Each of the first nine digits of the ten-digit ISBN—excluding the check digit itself—is multiplied by its (integer) weight, 
            // descending from 10 to 2, and the sum of these nine products found
            isbnDigits = isbnDigits.reverse().map((digit, index) => {
                return digit * (index + 2);
            });
            let s = isbnDigits.reduce((a, b) => a + b, 0);
            let cd = 11 - (s % 11);
            
            return lastDigit == (cd !== 10 ? cd : 'X'); 
        }
        else if(isbnDigits.length == 12) //isbn-13 algorithm
        {
            //begins with the first 12 digits of the thirteen-digit ISBN (thus excluding the check digit itself). 
            //each digit, from left to right, is alternately multiplied by 1 or 3, then those products are summed modulo 10 to give a value ranging from 0 to 9. 
            //subtracted from 10, that leaves a result from 1 to 10. A zero (0) replaces a ten (10), so, in all cases, a single check digit results.
            isbnDigits = isbnDigits.map( ( digit, index ) => {
                if ((index + 1) % 2 == 0)
                {
                    return digit * 3;
                }

                return digit;
            });

            //Calculate checksum from array:
            const sum = isbnDigits.reduce((a, b) => a + b, 0);
    
            //Validate control digit:
            const controlDigit = 10 - (sum % 10);
            return lastDigit === (controlDigit !== 10 ? controlDigit : 'X'); 
        }
        else {
            return false;
        }
    }

    public validate(value: any) {        
        if(!this.regexp.test(value) ) {
            return false;
        }
        
        return value && this.checksum(value);
    }
}
