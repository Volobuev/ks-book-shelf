import IValidator from "./validator";
import * as _ from 'lodash';

/**
 * Require validation
 */
export default class RequiredValidator implements IValidator {    
    public validate(value: any) {
        if (!_.isUndefined(value) && !_.isNull(value))
        {
            if (_.isString(value))
                return value != '' && value.length != 0 && !(/^\\s+$/.test(value));
            
            if (_.isNumber(value))
                return value != 0;
        }

        return false;
    }
}
