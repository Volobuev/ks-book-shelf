import IValidator from "./validator";
import * as moment from 'moment';

/**
 * Date format validation
 */
export default class DateValidator implements IValidator {    
    public validate(value: any) {
        let lowBorderDate = moment('01.01.1800', 'DD.MM.YYYY', true);
        let date = moment(value, 'DD.MM.YYYY', true);
        return date.isValid() && date >= lowBorderDate; 
    }
}
