import IValidator from "./validator";
import * as _ from 'lodash';

/**
 * String length validation
 */
export default class NumberRangeValidator implements IValidator {
    private from;
    private to;

    constructor(from: number = -1, to: number = -1) {
        this.from = from;
        this.to = to;
    }
    
    public validate(value: any) {
        if (_.isNumber(value)) {
            if (this.to < 0 && this.from > 0)
                return value >= this.from;

            if (this.to > 0 && this.from < 0)
                return value <= this.to;

            if (this.to >= 0 && this.from >= 0)
                return value >= this.from && value <= this.to;
        }

        return true; 
    }
}
