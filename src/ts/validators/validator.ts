export default interface IValidator {
    validate(value: any): boolean;
}
